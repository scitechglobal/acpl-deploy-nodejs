var common = require("../common.js");
var secrets = common.secrets;

var assert = require("assert");

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiAsPromised = require('chai-as-promised');

var expect = chai.expect;
var request = chai.request;
var should = chai.should();

chai.use(chaiHttp);
chai.use(chaiAsPromised);

function run() {
	
	describe('SAP Thing Services', function(){
		
		it('get ZHCM', function(done) {
			done();			
		});

		it('get ZHCMGR', function(done) {
			done();			
		});

		it('get ZPDT', function(done) {
			done();			
		});

		it('get COID', function(done) {
			done();			
		});

		it('get SSRGN', function(done) {
			done();			
		});

		it('get DESPLAN', function(done) {	
			done();		
		});

		it('get MELCSV', function(done) {	
			done();		
		});

	});

}

module.exports = {
	run: run
};