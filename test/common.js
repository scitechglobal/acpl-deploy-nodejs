var secureEnv = require('secure-env');
var envJson = secureEnv({secret:'df143cc1428826e4130674f975dd34d3326d16d5c781e26a8f880a6391d57c8df7602246e6e48c7776c67d1a5a6a8ce2'}); 

var environments = JSON.parse(envJson.environments);

var environment = process.env.npm_config_environment;

var secrets = environments[environment]

module.exports = {
	secrets: secrets
}

