var common = require("../common.js");
var sap = require("../sap/api.js")
var hcm = require("../modules/hcm.js")
var gelroom = require("../modules/gelroom.js")

var secrets = common.secrets;

var assert = require("assert");

var chai = require('chai');
var chaiHttp = require('chai-http');
var chaiAsPromised = require('chai-as-promised');

var expect = chai.expect;
var request = chai.request;
var should = chai.should();

chai.use(chaiHttp);
chai.use(chaiAsPromised);

var imported = false;
var importErrors = null;

function checkout() {
	describe('Checkout', function(){
		it('ACPL Git repository checkout', function(done) {
				chai.request(secrets.thingworxUrl)
					.post("/Things/AcplGitRepo/Services/Checkout")
					.set('Content-Type', 'application/json')
					.set('Accept', 'application/json')
					.set('appKey', secrets.thingworxAppKey)
					.send({ 'BranchNameOrCommit': secrets.branch })
					.end(function(err, res) {
	     				expect(res).to.have.status(200);
						done();
						
						//pull branch
						pull(); 
					})
			});

		after(function() {
		});
	});

}

function pull() {
	describe('Pull', function(){
		it('ACPL Git repository pull', function(done) {
			chai.request(secrets.thingworxUrl)
				.post("/Things/AcplGitRepo/Services/Pull")
				.set('Content-Type', 'application/json')
				.set('Accept', 'application/json')
				.set('appKey', secrets.thingworxAppKey)
				.send({})
				.end(function(err, res) {
     				expect(res).to.have.status(200);
					done();

					//import git repository to Thingworx
					importGitRepository();
				});
		});

		after(function() {		
		});

	});
}

function importGitRepository() {
	describe('Import Git Repository', function(){
		it('import source control entities', function(done) {
			chai.request(secrets.thingworxUrl)
				.post("/Resources/SourceControlFunctions/Services/ImportSourceControlledEntities")
				.set('Content-Type', 'application/json')
				.set('Accept', 'application/json')
				.set('appKey', secrets.thingworxAppKey)
				.send({ 'path': '/acpl-thingworx', 
					'repositoryName': 'GitRepository', 
					'useDefaultDataProvider': false, 
					'withSubsystems': false 
				})
				.end(function(err, res, body) {
					
					if(err) {
						this.importError = err.rawResponse;
						// assert.fail(err.rawResponse);
						done(new Error(err.rawResponse));	
					} 

					if(res) {	
						this.imported = (res.statusCode == 200);
						expect(res.statusCode).to.equal(200);
						done();
					}
					
				});
		});

		after(function() {

			if(this.imported) {
				sap.run();
				hcm.run();
				gelroom.run();
			}

		});
	});
}

function run() {
	checkout();
}

module.exports = {
	run: run
};